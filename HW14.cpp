#include <iostream>
#include <iomanip>
#include <string>

int main()
{

	//std::string mystring = "0123456789";
	//std::cout << mystring << "\n";
	//std::cout << mystring.length() << "\n";
	//std::cout << mystring[0] << "\n";
	//std::cout << mystring[mystring.length()-1] << "\n";	

	std::cout << "Type some numbers : ";
	std::string somenumbers;
	std::cin >> somenumbers;

	std::cout << somenumbers << "\n";
	std::cout << somenumbers.length() << "\n";
	std::cout << somenumbers[0] << "\n";
	std::cout << somenumbers[somenumbers.length() - 1] << "\n";

	return 0;

}